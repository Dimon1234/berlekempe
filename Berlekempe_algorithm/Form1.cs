﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Berlekempe_algorithm
{
    public partial class Form1 : Form
    {
        int p, n, N, l, lD, i, sIndex;
        int[] d, L, k, checkH;
        //int m = 2;
        int m = 4;
        int ArrayLength = 100;
        int[][] h, D;

        private void pCompoBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 00010443432210141121011003 использовать по умолчанию для примера 5ричной
            // 405531604514635154243 использовать по умолчанию для 7ричной
            if (pCompoBox.SelectedIndex == 3)
            {
                startTextBox.Text = "405531604514635154243";
            } else if (pCompoBox.SelectedIndex == 2)
            {
                startTextBox.Text = "00010443432210141121011003";
            }
        }

        string s;
        string Log;

        private void Form1_Load(object sender, EventArgs e)
        {
            // выбор по умолчанию 7ричной
            //pCompoBox.SelectedIndex = 2; // 5теричная
            pCompoBox.SelectedIndex = 3; // 7ричная
        }


        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Log = "Лог вычислений: \n";

            // Шаг 0 (установка стартовых значений)
            p = Convert.ToInt32(pCompoBox.SelectedItem.ToString());
            N = (int) Math.Pow(p, m) - 1;
            
            s = startTextBox.Text.ToString();

            // Ограничение
            for (i = 0; i < s.Length; i++)
            {
                if (Convert.ToInt32(s[i].ToString()) >= p)
                {
                    MessageBox.Show("Внимание! В исходной последовательности есть числа >= " + p + ". Исправьте эту проблему");
                    return;
                }
            }

            // Шаг 0 (установка стартовых значений)
            k = new int[ArrayLength];
            L = new int[ArrayLength];
            k[0] = -1;
            L[0] = 0;

            l = 0;

            h = new int[ArrayLength][];
            D = new int[ArrayLength][];

            for (i=0; i< ArrayLength; i++)
            {
                h[i] = new int[ArrayLength];
                D[i] = new int[ArrayLength];
            }
            h[0][0] = 1;
            D[0][0] = 0; D[0][1] = 1; lD = 0;

            d = new int[ArrayLength];
            d[0] = Convert.ToInt32(s[0].ToString());

            n = 0;

            // Каждый шаг алгоритма
            do
            {
                n++;
                l = 0;

                // нахождение h[n]
                for (i = 0; i <= n; i++)
                {
                    h[n][i] = h[n-1][i] - d[n-1] * D[n-1][i];
                    while (h[n][i] < 0)
                    {
                        h[n][i] = h[n][i] + p;
                    }

                    if (h[n][i] > 0)
                    {
                        l = Math.Max(i, l);
                    }
                }

                // Нахождение L[n]
                if (d[n-1] != 0)
                {
                    L[n] = Math.Max(L[n-1], n-1-k[n-1]);
                }
                else
                {
                    L[n] = L[n-1];
                }

                lD++;

                // Нахождение D[n]
                if (L[n] == L[n-1])
                {
                    k[n] = k[n - 1];
                    lD++;
                    for (i = lD; i > 0; i--)
                    {
                        D[n][i] = D[n-1][i-1];
                    }
                    
                    D[n][0] = 0;
                } else
                {
                    k[n] = n - 1 - L[n-1];
                    for (i = lD; i > 0; i--)
                    {
                        D[n][i] = DivisionOn(p, h[n-1][i - 1], d[n-1]);                       
                    }
                }

                // Нахождение d[n]
                d[n] = 0;

                for (i = l; i >= 0; i--)
                {
                    sIndex = n - i;

                    if (sIndex >= s.Length)
                    {
                        d[n] = 0;
                        break;
                    }

                    d[n]+= h[n][i] * Convert.ToInt32(s[sIndex].ToString());
                }

                d[n] = d[n] % 5;

                // Запись в лог вычислений
                Log += "n: " + n + " " + "\nh(n-1):";
                for (i = 0; i < 15; i++)
                {
                    Log += h[n-1][i] + " ";
                }
                Log += "\nh(n):";
                for (i = 0; i < 15; i++)
                {
                    Log += h[n][i] + " ";
                }
                Log += "\nD(n-1):";
                for (i = 0; i < 15; i++)
                {
                    Log += D[n-1][i] + " ";
                }
                Log += "\nD(n):";
                for (i = 0; i < 15; i++)
                {
                    Log += D[n][i] + " ";
                }

                Log += "\nl: " + l + " " + "\n";
                Log += "d(n -1): " + d[n - 1] + " " + "\n";
                Log += "d(n): " + d[n] + " " + "\n";
                Log += "L(n-1): " + L[n-1] + " " + "\n";
                Log += "L(n): " + L[n] + " " + "\n";
                Log += "k(n-1): " + k[n - 1] + " " + "\n";
                Log += "k(n): " + k[n] + " " + "\n";
                Log += "h[n] == h[n-1] : " + (this.EqualsCheck(h[n], h[n-1])) + "";
                Log += "\n_____________________________\n";

                // Условие для окончания алгоритма
                if (n >= 2 && L[n] > 0 && d[n] == 0)
                {
                    if (this.EqualsCheck(h[n], h[n - 1]) && this.EqualsCheck(h[n], h[n - 2]))
                    {
                        break;
                    }
                }

            } while (n < N);

            Log+= "FINISH \n";

            // Формирование и вывод результатов
            resultLabel.Text = OutputResult(h[n]);
            checkH = new int[ArrayLength];
            checkH = this.GetCheckH(h[n], L[n]);
            checkResult.Text = this.OutputResult(checkH);
            logText.Text = Log;
        }

        // нахождение сопряженного (проверочного) полинома
        public int[] GetCheckH (int[] h, int L)
        {
            for (i = 0; i <= L; i++)
            {
                checkH[i] = h[L - i];
            }
            return checkH;
        }

        // формирование вывода полинома
        public string OutputResult (int[] h)
        {
            string Result = "";
            bool noFirst = false;
            for (i = 0; i < h.Length; i++)
            {
                if (h[i] != 0)
                {
                    if (noFirst)
                    {
                        Result+= " + ";
                    } else
                    {
                        noFirst = true;
                    }
                    if (h[i] != 1 || i == 0)
                    {
                        Result+= "" + h[i];
                    }

                    if (i != 0)
                    {
                        Result += "x";
                    } 
                    

                    if (i > 1)
                    {
                        Result += "^" + i;
                    }
                }
            }
            return Result;
        }

        // проверка эквивалентности полиномов
        public bool EqualsCheck (int[] a, int[] b) {
            for (i = 0; i <= a.Length - 1; i++)
            {
                if (a[i] != b[i])
                {
                    return false;
                }
            }
            return true;
        }

        // "Деление" полинома на невязку 
        public int DivisionOn(int p, int dividend, int divider)
        {
            if (dividend == 0)
            {
                return 0;
            }

            while (dividend < divider || dividend % divider > 0)
            {
                dividend += p;
            }

            return dividend/divider;
        }
    }
}
